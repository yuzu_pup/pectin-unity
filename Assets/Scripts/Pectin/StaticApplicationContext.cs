using System.Collections;
using System.Collections.Generic;
using Pectin.Random;
using UnityEngine;

namespace Pectin
{
    /// <summary>
    /// Container for several static globals 
    /// </summary>
    public static class StaticApplicationContext
    {
        /// <summary>
        /// A random number generator that is used when a random number generator is not provided to some
        /// APIs
        /// </summary>
        public static readonly RandomNumberGenerator DefaultGlobalRandomNumberGenerator = new SystemRandomRNG();
    }
}
