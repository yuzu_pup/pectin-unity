using System.Collections;
using System.Collections.Generic;
using Pectin.Random;
using UnityEngine;

namespace Pectin.Random
{
    /// <summary>
    /// A random number generator whose generation method is seeded using an integer. Two instances of the same
    /// implementation of this interface should produce the same random values in the same order
    /// </summary>
    public interface SeededRandomNumberGenerator : RandomNumberGenerator
    {
        /// <summary>
        /// Sets the seed value of this random number generator
        /// </summary>
        int Seed { set; }
    }

}
