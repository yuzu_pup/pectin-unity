using System;

namespace Pectin.Random
{
    public class SystemRandomRNG : SeededRandomNumberGenerator
    {
        private System.Random _Instance;

        public SystemRandomRNG() : this((int) System.DateTime.Now.Ticks)
        {
            
        }
        
        public SystemRandomRNG(int seed)
        {
            _Instance = new System.Random(seed);
        }
        
        public int Seed
        {
            set
            {
                _Instance = new System.Random(value);
            }
        }

        public float Value() => (float) _Instance.NextDouble();

        public float ValueInRange(float minInclusive, float maxExclusive)
        {
            return Value() * (minInclusive - maxExclusive) + minInclusive;
        }

        public int ValueInRange(int minInclusive, int maxExclusive) => _Instance.Next(minInclusive, maxExclusive);
    }

}
