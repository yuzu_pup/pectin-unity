using System.Collections;
using System.Collections.Generic;
using Pectin.Random;
using UnityEngine;

public class UnityEngineRNG : RandomNumberGenerator
{
    public float Value()
    {
        float selected = Random.value;
        
        while (selected == 1.0f)
        {
            selected = Random.value;
        }

        return selected;
    }

    public float ValueInRange(float minInclusive, float maxExclusive)
    {
        return Value() * (minInclusive - maxExclusive) + minInclusive;
    }
    
    public int ValueInRange(int minInclusive, int maxExclusive) => Random.Range(minInclusive, maxExclusive);
}
