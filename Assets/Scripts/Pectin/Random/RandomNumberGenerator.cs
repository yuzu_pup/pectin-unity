﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pectin.Random
{
    /// <summary>
    /// Something capable of generating random numbers
    /// </summary>
    public interface RandomNumberGenerator
    {
        /// <returns>A random value in the range [0.0, 1.0)</returns>
        float Value();

        /// <param name="minInclusive">The smallest value in the range that can be selected</param>
        /// <param name="maxExclusive">The largest value in the range that can be selected</param>
        /// <returns>A value in the range [<paramref name="minInclusive"/>, <paramref name="maxExclusive"/>)</returns>
        float ValueInRange(float minInclusive, float maxExclusive);

        /// <param name="minInclusive">The smallest value in the range that can be selected</param>
        /// <param name="maxExclusive">The largest value in the range that can be selected</param>
        /// <returns>A value in the range [<paramref name="minInclusive"/>, <paramref name="maxExclusive"/>)</returns>
        int ValueInRange(int minInclusive, int maxExclusive);
    }

}
