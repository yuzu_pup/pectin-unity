# About
Pectin is a collection of small utilities classes and other things I've found useful that I maintain for game jams where I use Unity. Hopefully they'll be of use to you too!

A few important things to note: 
* Pectin is specifically used for **game jams**. It should not be included as-is in production level code.
* Pectin's focus is on making code and Unity components **understandable and easy to read**. Compromises in speed, efficiency, and in some cases, correctness are acceptible.
* Hacks may be employed in the name of ease-of-use. The goal is to make common needs quick to implement.

# Contents
Pectin has the following namespaces:
* Extensions - contains various extension methods for standard C# classes
* Random - A small framework for random number generators, with bindings for System.Random and UnityEngine.Random
  * Random.Extensions - Extends standard C# collections with methods for getting random items

## Why the name "pectin"?
Pectin is an ingredients in jams and jellies that is a key part of making them gel and come together! I hope that it can help your project come together as well.